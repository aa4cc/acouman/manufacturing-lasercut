# Manufacturing

## Laser-cut

The drawings were originally made in Autodesk Fusion and exported to DXF.
The drawings are split into layers, named _Cut_ or _Engrave_.
We made the laser-cut parts using Epilog Mini 18.
